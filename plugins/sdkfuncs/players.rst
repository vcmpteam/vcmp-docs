****************
Player functions
****************

Player information
==================

Player worlds
=============

Player class settings
=====================

Player spawn cycle
==================

Player statistics
=================

Player health and immunity
==========================

Player position and rotation
============================

Player actions
==============

Player vehicles
===============

Player weapons
==============

Player camera
=============

Miscellaneous player functions
==============================
