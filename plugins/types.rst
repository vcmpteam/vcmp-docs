.. highlight:: c

*********************
SDK type declarations
*********************

Macro definitions
=================

.. c:macro:: PLUGIN_API_MAJOR

   The major version number used by the plugin SDK. The server will refuse to
   load plugins that do not use the same major version of the SDK. Changes to
   the major version indicate breaking changes.

.. c:macro:: PLUGIN_API_MINOR

   The minor version number used by the plugin SDK. The server will load plugins
   with different minor versions as long as :c:macro:`PLUGIN_API_MAJOR` is
   compatible. SDKs with newer minor versions typically have added properties
   but do not remove or break types or function declarations and are thus
   backwards-compatible.


ServerSettings
==============

.. c:type:: ServerSettings

   Used by :c:func:`PluginFuncs.GetServerSettings()` to provide information
   about basic server settings to the plugin.

.. c:member:: uint32_t ServerSettings.structSize

   The size of the :c:type:`ServerSettings` struct in bytes. Useful for
   checking if extended properties are present. For example, if a new API
   version adds a new field but you'd like to retain backwards-compatibility
   with older servers, you can check the size of this struct before referencing
   the new field.

.. c:member:: char[128] ServerSettings.serverName

   The name of the server. Also accessible via
   :c:func:`PluginFuncs.GetServerName()` and
   :c:func:`PluginFuncs.SetServerName()`.

.. c:member:: uint32_t ServerSettings.maxPlayers

   The maximum number of players that can connect to the server. Also accessible
   via :c:func:`PluginFuncs.GetMaxPlayers` and
   :c:func:`PluginFuncs.SetMaxPlayers`.

.. c:member:: uint32_t ServerSettings.port

   The port the server is listening on. This field cannot be changed while the
   server is running.

.. c:member:: uint32_t ServerSettings.flags

   Reserved. Currently always set to zero.


PluginInfo
==========

.. c:type:: PluginInfo

   Describes a plugin so that it can be looked up and referenced by other
   plugins. Should only be altered in :c:func:`VcmpPluginInit()`, but can be
   searched for in plugin functions.

.. c:member:: uint32_t PluginInfo.structSize

   The size of the :c:type:`PluginInfo` struct in bytes. Useful for checking
   if extended properties are present. For example, if a new API version adds
   a new field but you'd like to retain backwards-compatibility with older
   servers, you can check the size of this struct before referencing the new
   field.

   This property is set by the server and should not be altered.

.. c:member:: uint32_t PluginInfo.pluginId

   A unique 32-bit identifier for the plugin. The server assigns pluginId
   starting at zero and increasing it for each plugin loaded.

   This property is set by the server and should not be altered.

.. c:member:: char[32] PluginInfo.name

   A name for the plugin. Can be used to search for the plugin using
   :c:func:`PluginFuncs.FindPlugin()`.

   By default this is the filename of the plugin without the file extension,
   but can be edited to make it more readily searchable.

.. c:member:: uint32_t PluginInfo.pluginVersion

   A version number for the plugin. Can be used by other plugins to check for
   compatibility.

.. c:member:: uint16_t PluginInfo.apiMajorVersion

   The major version of the plugin SDK that this plugin supports. Should be set
   to :c:macro:`PLUGIN_API_MAJOR` in new/updated plugins for the plugin to load.

.. c:member:: uint16_t PluginInfo.apiMinorVersion

   The minor version of the plugin SDK that this plugin supports. Should be set
   to :c:macro:`PLUGIN_API_MINOR` in new/updated plugins for maximum
   compatibility.


vcmpError
=========

.. c:type:: vcmpError

   An enumeration of error codes that may be returned by plugin functions.

.. c:var:: int32_t vcmpErrorNone

   No errors have occurred.

.. c:var:: int32_t vcmpErrorNoSuchEntity

   For plugin functions that take an entity ID (e.g. players, pickups, objects),
   indicates that no entity could be found with the ID given.

.. c:var:: int32_t vcmpErrorBufferTooSmall

   For plugin functions that take a char array/char pointer for storing strings,
   indicates that the buffer is too small to fit the string being copied.

.. c:var:: int32_t vcmpErrorTooLargeInput

   A char array used for setting a string value was too large to be copied.

.. c:var:: int32_t vcmpErrorArgumentOutOfBounds

   An argument's value is not within the accepted range.

.. c:var:: int32_t vcmpErrorNullArgument

   An argument was null when some valid pointer was expected.

.. c:var:: int32_t vcmpErrorPoolExhausted

   For plugin functions that create entities, indicates that no more entities of
   a given type can be created.

.. c:var:: int32_t vcmpErrorInvalidName

    Tried to set a player's name to a different value but the name contained
    invalid characters.

.. c:var:: int32_t vcmpErrorRequestDenied

    The server asked an entity to perform an action, but the entity refused to
    do so, likely because it was doing something else.

.. c:var:: int32_t forceSizeVcmpError

    Reserved. Only used to force the :c:type:`vcmpError` enum to be at least as
    large as an int32_t.


vcmpEntityPool
==============

.. c:type:: vcmpEntityPool

   An enumeration of entity pool types. Entity pools keep track of all instances
   of entities that can be created and destroyed by the server.

.. c:var:: int32_t vcmpEntityPoolVehicle

   The vehicle pool.

.. c:var:: int32_t vcmpEntityPoolObject

   The object pool.

.. c:var:: int32_t vcmpEntityPoolPickup

   The pickup pool.

.. c:var:: int32_t vcmpEntityPoolRadio

   The custom radio station pool.

.. c:var:: int32_t vcmpEntityPoolBlip

   The map blip pool.

.. c:var:: int32_t vcmpEntityPoolCheckpoint

   The checkpoint/sphere pool.

.. c:var:: int32_t forceSizeVcmpEntityPool

   See :c:data:`forceSizeVcmpError`.


vcmpDisconectReason
===================

.. c:type:: vcmpDisconnectReason

   An enumeration of reasons a player may have been disconnected from the server
   for.

.. c:var:: int32_t vcmpDisconnectReasonTimeout

   The player did not respond to pings in a timely fashion and so the server has
   assumed their connection has been lost.

.. c:var:: int32_t vcmpDisconnectReasonQuit

   The player quit using the /q command or by exiting through the menu.

.. c:var:: int32_t vcmpDisconnectReasonKick

   The player was kicked by the server.

.. c:var:: int32_t vcmpDisconnectReasonCrash

   The player's game crashed and failed to recover.

.. c:var:: int32_t vcmpDisconnectReasonAntiCheat

   The player was flagged for cheating.

.. c:var:: int32_t forceSizeVcmpDisconnectReason

   See :c:data:`forceSizeVcmpError`.


vcmpEntityPool
==============

.. c:type:: vcmpBodyPart

   An enumeration of body parts that may be reported when a player is shot or
   killed.

.. c:var:: int32_t vcmpBodyPartBody

   Somewhere on the body, exact location undetermined.

.. c:var:: int32_t vcmpBodyPartTorso

   The torso.

.. c:var:: int32_t vcmpBodyPartLeftArm

   The left arm.

.. c:var:: int32_t vcmpBodyPartRightArm

   The right arm.

.. c:var:: int32_t vcmpBodyPartLeftLeg

   The left leg.

.. c:var:: int32_t vcmpBodyPartRightLeg

   The right leg.

.. c:var:: int32_t vcmpBodyPartHead

   The head.

.. c:var:: int32_t vcmpBodyPartInVehicle

   The player was in a vehicle when they were shot/killed.

.. c:var:: int32_t forceSizeVcmpBodyPart

   See :c:data:`forceSizeVcmpError`.


vcmpPlayerState
===============

.. c:type:: vcmpPlayerState

   An enumeration of player states that may be returned by
   :c:func:`PluginFuncs.GetPlayerState()`.

.. c:var:: int32_t vcmpPlayerStateNone

   The player is not currently being synced.

.. c:var:: int32_t vcmpPlayerStateNormal

   The player is on-foot.

.. c:var:: int32_t vcmpPlayerStateAim

   The player is firing a weapon or aiming through a scoped weapon.
   This player state has aim position and aim direction data associated with it.
   See :c:func:`PluginFuncs.GetPlayerAimPosition()` and
   :c:func:`PluginFuncs.GetPlayerAimDirection()` for more information.

.. c:var:: int32_t vcmpPlayerStateDriver

   The player is driving a vehicle.

.. c:var:: int32_t vcmpPlayerStatePassenger

   The player is a passenger in a vehicle.

.. c:var:: int32_t vcmpPlayerStateEnterDriver

   The player is entering a car as a driver.

.. c:var:: int32_t vcmpPlayerStateEnterPassenger

   The player is entering a car as a passenger.

.. c:var:: int32_t vcmpPlayerStateExit

   The player is exiting a vehicle.

.. c:var:: int32_t vcmpPlayerStateUnspawned

   The player is not spawned.

.. c:var:: int32_t forceSizeVcmpPlayerState

   See :c:data:`forceSizeVcmpError`.


vcmpPlayerUpdate
================

.. c:type:: vcmpPlayerUpdate

   An enumeration of player update types passed to
   :c:func:`PluginCallbacks.OnPlayerUpdate()`.

.. c:var:: int32_t vcmpPlayerUpdateNormal

   The player is on-foot being synced as a normal ped.

.. c:var:: int32_t vcmpPlayerUpdateAiming

   The player is firing a weapon or aiming through a scoped weapon.
   This sync type has aim position and aim direction data associated with it.
   See :c:func:`PluginFuncs.GetPlayerAimPosition()` and
   :c:func:`PluginFuncs.GetPlayerAimDirection()` for more information.

.. c:var:: int32_t vcmpPlayerUpdateDriver

   The player is driving a vehicle. Coincides with a
   :c:data:`vcmpVehicleUpdateDriverSync` update.

.. c:var:: int32_t vcmpPlayerUpdatePassenger

   The player is a vehicle passenger.

.. c:var:: int32_t forceSizeVcmpPlayerUpdate

   See :c:data:`forceSizeVcmpError`.


vcmpPlayerVehicle
=================

.. c:type:: vcmpPlayerVehicle

   An enumeration of players' vehicle entry states as reported by
   :c:func:`PluginFuncs.GetPlayerInVehicleStatus()`.

.. c:var:: int32_t vcmpPlayerVehicleOut

   The player is not in a vehicle.

.. c:var:: int32_t vcmpPlayerVehicleEntering

   The player is entering a vehicle.

.. c:var:: int32_t vcmpPlayerVehicleExiting

   The player is exiting a vehicle.

.. c:var:: int32_t vcmpPlayerVehicleIn

   The player is in a vehicle.

.. c:var:: int32_t forceSizeVcmpPlayerVehicle

   See :c:data:`forceSizeVcmpError`.


vcmpVehicleSync
===============

.. c:type:: vcmpVehicleSync

   An enumeration of vehicles' sync states as reported by
   :c:func:`PluginFuncs.GetVehicleSyncType()`.

.. c:var:: int32_t vcmpVehicleSyncNone

   The vehicle is not being synced by anyone.

.. c:var:: int32_t vcmpVehicleSyncDriver

   The vehicle is being driven and is synced by its driver.

.. c:var:: int32_t vcmpVehicleSyncPassenger

   The vehicle has no driver but is primarily synced by one of its passengers.

.. c:var:: int32_t vcmpVehicleSyncNear

   The vehicle has no occupants but is being synced by a nearby player.

.. c:var:: int32_t forceSizeVcmpVehicleSync

   See :c:data:`forceSizeVcmpError`.


vcmpVehicleUpdate
=================

.. c:type:: vcmpVehicleUpdate

   An enumeration of vehicle update types passed to
   :c:func:`PluginCallbacks.OnVehicleUpdate`.

.. c:var:: int32_t vcmpVehicleUpdateDriverSync

    The vehicle's state is being updated by its driver. Coincides with a
    :c:data:`vcmpPlayerUpdateDriver` update.

.. c:var:: int32_t vcmpVehicleUpdateOtherSync

   The vehicle's state is being updated by a passenger or nearby player.

.. c:var:: int32_t vcmpVehicleUpdatePosition

   The vehicle's position was changed by a plugin via
   :c:func:`PluginFuncs.SetVehiclePosition()`.

.. c:var:: int32_t vcmpVehicleUpdateHealth

   The vehicle's health was changed by a plugin via
   :c:func:`PluginFuncs.SetVehicleHealth()`.

.. c:var:: int32_t vcmpVehicleUpdateColour

   The vehicle's primary or secondary colour was changed by a plugin via
   :c:func:`PluginFuncs.SetVehicleColour()`.

.. c:var:: int32_t vcmpVehicleUpdateRotation

   The vehicle's rotation was changed by a plugin via
   :c:func:`PluginFuncs.SetVehicleRotation()` or
   :c:func:`PluginFuncs.SetVehicleRotationEuler()`.

.. c:var:: int32_t forceSizeVcmpVehicleUpdate

   See :c:data:`forceSizeVcmpError`.


vcmpServerOption
================

.. c:type:: vcmpServerOption

   An enumeration of boolean (on/off) server settings that can be configured
   via :c:func:`PluginFuncs.SetServerOption`.

.. c:var:: int32_t vcmpServerOptionSyncFrameLimiter

   **Default**: off

   Controls whether or not the frame limiter graphics setting is synced.
   Enabling this option forces players' games to respect the frame limiter
   status controlled by :c:data:`vcmpServerOptionFrameLimiter`.

.. warning::

   While Vice City looks smoother at 60fps, parts of the game (e.g. physics
   simulations, looping sounds) will not behave consistently above 30fps. If
   consistency is critical, such as in a competitive deathmatch server, you
   should sync the frame limiter setting regardless of whether you prefer to
   have it on or off.

.. c:var:: int32_t vcmpServerOptionFrameLimiter

   **Default**: off

   Controls whether or not the frame limiter should be enabled for players.

.. note::

   Disabling the frame limiter will still limit players to 60fps at most due
   to game engine limitations.

.. c:var:: int32_t vcmpServerOptionTaxiBoostJump

   **Default**: off

   Controls whether taxis can boost jump by pressing the horn key, as is
   possible in single-player by completing 100 taxi fares.

.. c:var:: int32_t vcmpServerOptionDriveOnWater

   **Default**: off

   Controls whether cars can drive on water, as is possible in single-player
   by using the SEAWAYS cheat.

.. c:var:: int32_t vcmpServerOptionFastSwitch

   **Default**: off

   Controls whether or not players can switch weapons while in the middle of
   a shooting animation.

.. c:var:: int32_t vcmpServerOptionFriendlyFire

   **Default**: on

   Controls whether or not players on the same team do damage to each other.

.. c:var:: int32_t vcmpServerOptionDisableDriveBy

   **Default**: off

   Controls whether or not drive-bys are disabled.

.. c:var:: int32_t vcmpServerOptionPerfectHandling

   **Default**: off

   Controls whether or not vehicle turn handling and traction are incresed,
   as is possible in single-player with the GRIPISEVERYTHING cheat.

.. c:var:: int32_t vcmpServerOptionFlyingCars

   **Default**: off

   Controls whether or not cars can fly, as is possible in single-player with
   the COMEFLYWITHME cheat.

.. c:var:: int32_t vcmpServerOptionJumpSwitch

   **Default**: on

   Controls whether or not players can change weapons while jumping in the air.

.. c:var:: int32_t vcmpServerOptionShowMarkers

   **Default**: on

   Controls whether or not players appear on the radar to other players.

.. c:var:: int32_t vcmpServerOptionOnlyShowTeamMarkers

   **Default**: off

   Controls whether or not players only see markers of players who are on the
   same team.

.. note:

   Disabling :c:data:`vcmpServerOptionShowMarkers` will override this setting.

.. c:var:: int32_t vcmpServerOptionStuntBike

   **Default**: off

   If enabled, players will not be able to fall off of bikes.

.. c:var:: int32_t vcmpServerOptionShootInAir

   **Default**: off

   Controls whether or not players can shoot weapons in mid-air.

.. c:var:: int32_t vcmpServerOptionShowNameTags

   **Default**: on

   Controls whether or not nametags are enabled.

.. c:var:: int32_t vcmpServerOptionJoinMessages

   **Default**: on

   Controls whether or not the server automatically sends a chat message when
   a player joins.

.. c:var:: int32_t vcmpServerOptionDeathMessages

   **Default**: on

   Controls whether or not the server automatically sends a chat message when
   a player dies.

.. c:var:: int32_t vcmpServerOptionChatTagsEnabled

   **Default**: on

   Controls whether or not players can use color tags like [#FF0000] in their
   chat messages.

.. c:var:: int32_t vcmpServerOptionUseClasses

   **Default**: on

   Controls whether or not the server uses class selection. Enabling class
   selection allows you to set up a spawn screen with different classes
   that spawn in configurable locations with configurable weapons.

.. c:var:: int32_t vcmpServerOptionWallGlitch

   **Default**: off

   Controls whether or not players can wallglitch. The game engine can allow
   players to shoot through walls in some cases, a glitch known as
   wallglitching. If disabled, the game applies additional checks to make sure
   the player cannot shoot through obstacles.

.. c:var:: int32_t vcmpServerOptionDisableBackfaceCulling

   **Default**: off

   Controls whether or not backface culling is disabled. Backface culling is
   a game engine optimization where the game does not render polygons facing
   away from the camera. Some objects, such as those imported from GTA3 maps,
   do not behavior correctly is backface culling is enabled.

.. c:var:: int32_t vcmpServerOptionDisableHeliBladeDamage

   **Default**: off

   Controls whether or not damage from helicopter blades is disabled.

.. c:var:: int32_t forceSizeVcmpServerOption

   See :c:data:`forceSizeVcmpError`.


vcmpPlayerOption
================

.. c:type:: vcmpPlayerOption

   An enumeration of boolean (on/off) player settings that can be configured
   via :c:func:`PluginFuncs.SetPlayerOption`.

.. c:var:: int32_t vcmpPlayerOptionControllable

   **Default**: on

   Controls whether or not the player can control their character (i.e. move
   around, fire weapons). If disabled the player is frozen but can still use
   commands and chat.

.. c:var:: int32_t vcmpPlayerOptionDriveBy

   **Default**: on

   Controls whether or not the player can perform drivebys.

.. note::

   :c:data:`vcmpServerOptionDisableDriveBy` overrides this setting if enabled.

.. c:var:: int32_t vcmpPlayerOptionWhiteScanlines

   **Default**: off

   Controls whether or not the player has white VCR scanlines on the screen.

.. c:var:: int32_t vcmpPlayerOptionGreenScanlines

   **Default**: off

   Controls whether or not the player has green scanlines on the screen.

.. c:var:: int32_t vcmpPlayerOptionWidescreen

   **Default**: off

   Controls whether or not the player has a cinematic widescreen effect like
   in single-player mission cutscenes. Enabling this also disables the HUD.

.. c:var:: int32_t vcmpPlayerOptionShowMarkers

   **Default**: on

   Controls whether or not this player can see other players' markers on the
   radar and map.

.. c:var:: int32_t vcmpPlayerOptionCanAttack

   **Default**: on

   Controls whether or not this player can use any weapons, including fists.

.. c:var:: int32_t vcmpPlayerOptionHasMarker

   **Default**: on

   Controls whether or not other players can see this player on the radar and
   map.

.. c:var:: int32_t vcmpPlayerOptionChatTagsEnabled

   **Default**: on

   Controls whether or not this player can use color tags like [#FF0000] in
   chat.

.. note:: :c:data:`vcmpServerOptionChatTagsEnabled` overrides this setting.

.. c:var:: int32_t vcmpPlayerOptionDrunkEffects

   **Default**: off

   Controls whether or not this player has a drunk haze effect on their screen.

.. c:var:: int32_t forceSizeVcmpPlayerOption

   See :c:data:`forceSizeVcmpError`.


vcmpVehicleOption
=================

.. c:type:: vcmpVehicleOption

   An enumeration of boolean (on/off) vehicle settings that can be configured
   via :c:func:`PluginFuncs.SetVehicleOption`.

.. c:var:: int32_t vcmpVehicleOptionDoorsLocked

   **Default**: off

   Controls whether or not the doors are locked. If enabled, players will not
   be able to enter the vehicle by themselves.

.. c:var:: int32_t vcmpVehicleOptionAlarm

   **Default**: off

   Controls whether or not the car alarm is currently enabled.

.. c:var:: int32_t vcmpVehicleOptionLights

   **Default**: off

   Controls whether or not the vehicle's high beams are enabled.

.. c:var:: int32_t vcmpVehicleOptionRadioLocked

   **Default**: off

   If enabled, the driver will not be able to change the radio station by
   themselves. :c:func:`PluginFuncs.SetVehicleRadio()` can still be used to
   change the radio station.

.. c:var:: int32_t vcmpVehicleOptionGhost

   **Default**: off

   If enabled, this vehicle will not collide with other vehicles.

.. c:var:: int32_t vcmpVehicleOptionSiren

   **Default**: off

   Controls whether or not the vehicle's siren is enabled. Only applicable to
   emergency vehicles like firetrucks, ambulances and police cars.

.. c:var:: int32_t vcmpVehicleOptionSingleUse

   **Default**: off

   Controls whether or not the vehicle is a single-use entity. If enabled, the
   vehicle will delete itself instead of respawning after the vehicle is
   destroyed.

.. c:var:: int32_t forceSizeVcmpVehicleOption

   See :c:data:`forceSizeVcmpError`.


vcmpPickupOption
================

.. c:type:: vcmpPickupOption

   An enumeration of boolean (on/off) pickup settings that can be configured
   via :c:func:`PluginFuncs.SetPickupOption`.

.. c:var:: int32_t vcmpPickupOptionSingleUse

   **Default**: off

   Controls whether or not the pickup is a single-use entity. If enabled, the
   pickup will delete itself instead of respawning after being picked up.

.. c:var:: int32_t forceSizeVcmpPickupOption

   See :c:data:`forceSizeVcmpError`.
