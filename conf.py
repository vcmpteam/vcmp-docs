from pygments.lexer import RegexLexer, bygroups, include
from pygments.token import *
from sphinx.highlighting import lexers

# Adapted from https://github.com/jneen/rouge/pull/814
class SquirrelLexer(RegexLexer):
    name = 'Squirrel'
    aliases = ['squirrel', 'sq']
    filenames = ['*.nut']

    tokens = {
        'whitespace': [
            (r'\s+', Text),
            (r'(#|//).*$', Comment.Singleline),
            (r'/(\\\n)?[*][\w\W]*?[*](\\\n)?/', Comment.Multiline),
            (r'/(\\\n)?[*][\w\W]*', Comment.Multiline)
        ],
        'nest': [
            ('{', Punctuation, '#push'),
            ('}', Punctuation, '#pop'),
            include('root')
        ],
        'splice_string': [
            ('\\.', String),
            ('{', Punctuation, 'nest'),
            ('"|\n', String, '#pop'),
            ('.', String)
        ],
        'splice_literal': [
            ('""', String),
            ('{', Punctuation, 'nest'),
            ('"', String, '#pop'),
            ('.', String)
        ],
        'root': [
            include('whitespace'),

            # Strings
            (r'[$]\s*', String, 'splice_string'),
            (r'[$]@\s*', String, 'splice_literal'),
            ('@"(""|[^"])*', String),
            ('"(\\.|.)*?["\n]', String),
            ("'(\\.|.)'", String.Char),

            # Naming
            (r'\b(?:class)\b', Keyword, 'class_name'),
            (r'\b(?:function)\b', Keyword, 'function_name'),
            (r'\b(?:local)\b', Keyword.Declaration, 'variable_name'),
            (r'\b(?:enum)\b', Keyword, 'enum_name'),
            (r'\b(?:const)\b', Keyword, 'const_name'),

            # Reserved words
            (r'(base|break|case|catch|clone|continue|default|delete|do|else|extends|for|foreach|if|in|instanceof|resume|return|static|switch|throw|try|typeof|while|yield)\b', Keyword),
            (r'(class|function|local)\b', Keyword.Declaration),
            (r'(true|false|null)\b', Keyword.Constant),
            (r'(__LINE__|__FILE__)\b', Name.Variable.Magic),
            (r'(constructor)\b', Name.Function.Magic),
            (r'(this)\b', Name.Builtin.Pseudo),

            # Operators and punctuation
            (r'[~!%^&*()+=|\[\]{}:;,.<>\/?-]', Punctuation),

            # Numbers
            (r'(?i)(\d*\.\d+|\d+\.\d*)(e[+-]?\d+)?', Number.Float),
            (r'(?i)\d+e[+-]?\d+', Number.Float),
            (r'(?i)0x[0-9a-f]+', Number.Hex),
            (r'(?i)0[0-9]+', Number.Octal),
            (r'\d+', Number.Integer),

            ('[a-zA-Z_]+[a-zA-Z_0-9]*', Name)
        ],
        'class_name': [
            include('whitespace'),
            ('[a-zA-Z_]+[a-zA-Z_0-9]+(\.?[a-zA-Z_]+[a-zA-Z_0-9]+)*', Name.Class, '#pop')
        ],
        'function_name': [
            include('whitespace'),
            ('((::)?[a-zA-Z_]+[a-zA-Z_0-9]*)*', Name.Function, '#pop')
        ],
        'variable_name': [
            include('whitespace'),
            ('[a-zA-Z_]+[a-zA-Z_0-9]*', Name, '#pop')
        ],
        'const_name': [
            include('whitespace'),
            ('[a-zA-Z_]+[a-zA-Z_0-9]*', Name, '#pop')
        ],
        'enum_name': [
            include('whitespace'),
            ('[a-zA-Z_]+[a-zA-Z_0-9]*', Name, '#pop')
        ]
    }

lexers['squirrel'] = SquirrelLexer(startinline=True)

# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Vice City: Multiplayer'
copyright = '2018, the VC:MP development team'
author = 'VC:MP Development Team'

# The short X.Y version
version = '0.4'
# The full version, including alpha/beta/rc tags
release = '0.4.6'


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.githubpages',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path .
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_context = {
    'display_bitbucket': True,
    'bitbucket_user': 'vcmpteam',
    'bitbucket_repo': 'vcmp-docs',
    'bitbucket_version': 'master/'
}

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'ViceCityMultiplayerdoc'


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'ViceCityMultiplayer.tex', 'Vice City: Multiplayer Documentation',
     'The VC:MP Dev Team', 'manual'),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'vicecitymultiplayer', 'Vice City: Multiplayer Documentation',
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'ViceCityMultiplayer', 'Vice City: Multiplayer Documentation',
     author, 'ViceCityMultiplayer', 'One line description of project.',
     'Miscellaneous'),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']


# -- Extension configuration -------------------------------------------------

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True