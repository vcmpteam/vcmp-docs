**********************
Function documentation
**********************

VcmpPluginInit
==============

.. c:function:: uint32_t VcmpPluginInit(PluginFuncs* funcs, PluginCallbacks* callbacks, PluginInfo* info)

   The entry point for all server plugins. The server checks to see if a plugin
   exports this function; if present, the function is called so the plugin can
   set up callbacks, provide information about itself, and save the
   :c:type:`PluginFuncs` pointer in order to call SDK functions later.

   All plugins **should** populate :c:member:`PluginInfo.name`.

   :return: 1 on success, 0 on error

.. warning::

   Do not call attempt to create entities here and avoid performing any
   substantial logic here. You should instead use
   :c:func:`PluginCallbacks.OnServerInitialise()` to perform initialization.

   Failing to follow these guidelines may crash the server at startup.

.. note::

   When compiling for Windows, this function needs to be annotated with
   :samp:`__declspec(dllexport)`, like so:

   .. code-block:: c

      __declspec(dllexport) uint32_t VcmpPluginInit(...)
    
   Likewise, when compiling using C++, the function needs to be annotated
   with :samp:`extern "C"`, so a Windows plugin using C++ would look like
   this:

   .. code-block:: c

      extern "C" __declspec(dllexport) uint32_t VcmpPluginInit(...)
    
   When writing plugins in different languages for both Windows and Linux,
   it may be useful to use this macro to save time:

   .. code-block:: c

      #ifdef WIN32
          #define PLUGINSPEC __declspec(dllexport)
      #else
          #define PLUGINSPEC
      #endif

      #ifdef __cplusplus
          #define PLUGINLINKAGE extern "C"
      #else
          #define PLUGINLINKAGE
      #endif

      #define PLUGINEXPORT PLUGINLINKAGE PLUGINSPEC

      PLUGINEXPORT uint32_t VcmpPluginInit(...)


The PluginFuncs struct
======================

.. c:type:: PluginFuncs

   The struct that encapsulates all plugin function calls. The server populates
   this struct with function pointers that call into the server core to perform
   actions.

   A pointer to this struct is given in :c:func:`VcmpPluginInit()`. Your plugin
   should hold onto this pointer in order to call API functions. Your plugin
   must **not** change any of the function pointers in this struct.

.. c:member:: uint32_t PluginFuncs.structSize

   The size of the :c:type:`PluginFuncs` struct in bytes. Useful for checking
   if new functions added in a minor API version are present. If you'd like to
   retain backwards-compatibility with older servers, you can check the size of
   this struct before referencing newly added API functions.

Function details
================

.. toctree::
   :maxdepth: 3

   sdkfuncs/plugins
   sdkfuncs/messaging
   sdkfuncs/server
   sdkfuncs/environment
   sdkfuncs/weapons
   sdkfuncs/keybinds
   sdkfuncs/spawn
   sdkfuncs/administration
   sdkfuncs/players
   sdkfuncs/vehicles
   sdkfuncs/pickups-and-checkpoints
   sdkfuncs/objects