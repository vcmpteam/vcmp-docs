*************
About the mod
*************

**Vice City: Multiplayer** (commonly abbreviated as VC:MP) is a multiplayer mod
for PC versions of Grand Theft Auto: Vice City.

**********
Installing
**********

VC:MP can be installed by downloading the browser from the homepage_. The mod
requires a copy of GTA:VC, specifically retail v1.0 or a digital distribution
copy from a platform like Steam.

.. _homepage: https://www.vc-mp.org/